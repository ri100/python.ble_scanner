from service.ble_scanner import BleScanner
from service.collector import Collector
import argparse
from service.logger import LoggerWrapper

"""
    Scans for new BLE devices. Reports it to server immediately if new devices is discovered
    or after a delay defined by --send-every if it is not new i.e. is present for same time.
    Devices that disappears is removed from memory after 300 seconds. This time can be changed 
    by property remove_after_sec.
"""

parser = argparse.ArgumentParser()
parser.add_argument("--name", help="Scanner name", required=True)
parser.add_argument("--server", help="Central server ip address", required=True)
parser.add_argument("--port", help="Server port", default=1234, type=int)
parser.add_argument("--timeout", help="Server timeout in seconds", default=5, type=int)
parser.add_argument("--send-every", help="Send data to server every sec", default=60, type=int)

args = parser.parse_args()

""" 
    Collection manager is responsible for sending found ble devices to
    central server for analysis
"""
srv = (args.server, args.port)
thread = Collector(
    server=srv,             # where to send discovered ble devices
    delay=args.send_every,  # how often
    timeout=args.timeout    # timeout to server send/response command
)
thread.start()

logger = LoggerWrapper()

# Scans bluetooth for ble devices
try:

    scanner = BleScanner(
        server=srv, # used for immediate reporting of new ble devices
        scanner_name=args.name,
        # Defines devices to be watched. Defines custom names for devices.
        # Devices not defined are not being sent to server.
        watch_ble={
            'b8:d5:0b:ac:c1:2e': 'JBL-Charge-3',
            # 'cb:5a:bb:cc:cb:33': 'Risto-Keys',  # Trackr
            'e2:da:83:48:32:e1': 'Magda-Keys',
            'e9:0c:ed:40:39:75': 'Trackr3',
            '1d:34:92:ef:c4:62': 'Laptop-X240-Magda',
            '9c:b6:d0:d3:18:fe': 'Laptop-XPS13-Risto',
            'd4:80:9d:44:ab:f0': 'MX-Master-Mouse',
            '48:4b:aa:16:03:8d': 'iPhone-Risto',
            '31:15:7b:05:69:87': 'Kindle',
            '64:a7:69:60:1f:7d': 'HTC-Wildfire-Zygmunt',
            '34:7c:15:04:91:66': 'Waga-Media-Tech',
            '18:7a:93:05:1c:a3': 'Passat-Ble',  # Blue
            '18:7a:93:05:7e:bb': 'Risto-Keys'

        },
        timeout=15
    )
    scanner.scan()

except (KeyboardInterrupt, SystemExit):

    logger.warning("Waiting for scanner shutdown")
    thread.shutdown_flag.set()
    thread.join(timeout=thread.delay+10)

except Exception as e:
    logger.error("[ERR] %s" % e)
    error_log = LoggerWrapper('/var/log/ble_scanner_err.log')
    error_log.error(e)
    thread.shutdown_flag.set()
    thread.join(timeout=thread.delay+10)

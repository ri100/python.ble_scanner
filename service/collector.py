import copy
import threading
from service.logger import LoggerWrapper
from service.reporter import Reporter
from time import sleep

ble_devices = dict()
lock = threading.Lock()


class Collector(threading.Thread):
    """
    Collector is responsible for sending found ble devices to central server for analysis
    """

    def __init__(self, server: tuple, delay: int, timeout: int = 2):
        threading.Thread.__init__(self)
        self.reporter = Reporter(server, timeout)
        self.logger = LoggerWrapper()
        self.delay = delay
        self.shutdown_flag = threading.Event()
        self.last_timestamps = {}
        self.logger.info("\033[0;36m[INFO]\033[0m Collector delay set to %d" % delay)
        self.failed_send_to_server = {}

    def run(self):
        """
        Sends collected devices to server every x seconds.
        Delay is defined in delay variable
        :return:
        """
        while not self.shutdown_flag.is_set():
            with lock:
                # copy ble devices and release the lock
                copy_of_ble_devices = copy.deepcopy(ble_devices)

            for mac, ble in list(copy_of_ble_devices.items()):  # type:str, dict
                if not self._timestamp_changed(mac, ble):
                    self.logger.info(
                        "\033[0;36m[SKP]\033[0m %s Report to server skipped, device timestamp not changed" % mac)
                    continue

                ok = self.reporter.send_to_server(ble)

                if not ok:
                    self.failed_send_to_server[mac] = ble.copy()
                elif mac in self.failed_send_to_server:
                    del self.failed_send_to_server[mac]

                self.last_timestamps[mac] = ble['timestamp']

            sleep(5)

            for mac, ble in list(self.failed_send_to_server.items()):
                self.logger.info(
                    "\033[0;36m[INF]\033[0m %s Resending to server" % mac)

                ok = self.reporter.send_to_server(ble)

                if ok:
                    del self.failed_send_to_server[mac]
                    self.last_timestamps[mac] = ble['timestamp']

            if len(self.failed_send_to_server) == 0:
                delay = self.delay - 5
                if delay < 0:
                    delay = 5

                sleep(delay)

        self.logger.info("\033[0;36m[END]\033[0m Collector ends")

    def _timestamp_changed(self, mac, ble_device):
        return mac not in self.last_timestamps or self.last_timestamps[mac] < ble_device['timestamp']

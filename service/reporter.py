from time import sleep
from service.logger import LoggerWrapper
import socket


class Reporter:

    def __init__(self, server, timeout):
        self.logger = LoggerWrapper()
        self.server = server
        self.server_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self.server_client.settimeout(timeout)
        self.timeout = timeout

    def send_to_server(self, ble: dict) -> bool:

        scanner = ble['scanner']
        mac = ble['mac']
        timestamp = ble['timestamp']
        rssi = ble['rssi']
        name = ble['name']

        cmd = '[SET] %s %s %s %s %s' % (mac, rssi, name, timestamp, scanner)
        msg = str.encode(cmd)

        self.logger.info(
            "\033[0;34m[SET]\033[0m %s %s \033[1m%s\033[0m %s %s" % (mac, rssi, name, timestamp, scanner))

        try:

            response = self._send(msg, mac)
            if response is not None:
                self.logger.info("\033[0;34m[RSP]\033[0m " + response)
                return True

            return False

        except socket.timeout:
            srv, port = self.server
            self.logger.error("\033[0;31m[ERR]\033[0m %s Server %s:%s timeout in %d" % (mac, srv, port, self.timeout))
            # self._resend(mac, msg)

            return False

    def _resend(self, mac, msg):
        self.logger.info("\033[0;34m[RESENT]\033[0m %s" % mac)
        try:
            response = self._send(msg, mac)
            if response is not None:
                self.logger.info("\033[0;34m[RESENT]\033[0m " + response)
        except socket.timeout:
            self.logger.error("\033[0;31m[RESENT]\033[0m %s timeout" % mac)

    def _send(self, msg, mac):
        try:
            self.server_client.sendto(msg, self.server)
            msg_from_server = self.server_client.recvfrom(1024)
            return msg_from_server[0].decode("utf-8")
        except OSError as e:
            self.logger.error("\033[0;31m[ERR]\033[0m %s %s" % (mac, str(e)))
            sleep(3)
